-- sudo /opt/lampp/manager-linux-x64.run

-- MariaDB guide -> /opt/lampp/bin -> ./mysql -u root -p


-- Command to access MariaDB in cmd.
mysql -u root


-- Lists down the databases inside the DBMS/RDBMS
SHOW DATABASES;

-- Create a database;
CREATE DATABASE music_db;

-- Drop database;
DROP DATABASE music_db;

-- Select Database;
USE music_db;

SHOW TABLES;

-- Create Tables;
-- Table columns have the following forms : [column_name] [data_type] [other_options]

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY(id)
);

CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(100) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY(album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


CREATE TABLE playlists_songs (
	id INT NOT NULL,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT

);


CREATE TABLE reviews (
	id INT NOT NULL AUTO_INCREMENT,
	review VARCHAR(300) NOT NULL,
	PRIMARY KEY (id)
)

-- To display all the tables in a specific database.
SHOW TABLES;

-- To remove a specific table from a database.
-- DROP TABLE <table_name>
DROP TABLE reviews;

-- show the details of the table in a table format
-- DESCRIBE <table_name>
DESCRIBE reviews;

-- ALTER TABLE

-- to drop a column/field from a table.
-- ALTER TABLE <table_name> DROP COLUMN <field_name>
ALTER TABLE reviews DROP COLUMN review;

-- to add a new field in a table
-- ALTER TABLE <table_name> ADD <new_field> <DATA TYPE> NOT NULL;
ALTER TABLE reviews ADD comments VARCHAR(300) NOT NULL